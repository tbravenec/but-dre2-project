# Acoustic Echo Suppression

Term project for Brno University of Technology, class DRE2, focused on audio dereverberation using Weighted Prediction Error approach.

## Instructions

The application is multiplatform, and can be run on either Microsoft Windows or Linux based operating systems.
The instructions differ slightly for each system.

### Microsoft Windows
#### From source code
1) Download or clone this repository
2) Download and install Python 3.6 from [here][python_win] or 
directly download installer from [here][python_win_direct]
    - Make sure that "Add python to Path" button is checked during installation
    - In case you already have python installed you can check the version with command:

        ~~~~
        python --version
        ~~~~

3) Open command line and navigate to the root of the downloaded repository and run command:

    ~~~~
    python -m pip install numpy soundfile
    ~~~~    

    - This command will install all of the python packages required by the application

4) Launch the main.py from command line with:

    ~~~~
    python main.py {input_audio_file}
    ~~~~

    - This command will launch the application

#### From precompiled binaries
From the folder dist, the application can be launched directly using command:

~~~~
DeReverb.exe {input_audio_file}
~~~~

### Linux
Linux based systems usually have python already installed, unfortunatelly this does not make the installation easier.

1) Download or clone this repository
2) Open command line and run command:

    ~~~~
    pip3 install numpy soundfile
    ~~~~    

    - This command will install all of the python packages required by the application
    - If you want, you can also create virtual enviroment and install all packages there to keep the system installation of python clean

3) Launch the main.py from terminal with command:

    ~~~~
    python3 main.py {input_audio_file}
    ~~~~

    - This command will launch the application

## License and Third party code
The script is available under MIT license.

[python_win]: https://www.python.org/downloads/release/python-366/ "Python 3.6"
[python_win_direct]: https://www.python.org/ftp/python/3.6.6/python-3.6.6-amd64.exe "Python 3.6 installer"
