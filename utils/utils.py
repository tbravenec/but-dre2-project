import argparse
import os

import numpy as np
import soundfile as sf
from numpy.lib import stride_tricks


class DeReverb():
    def config(self, in_channels, out_channels, order=30, delay=2,
               frame_size=512, overlap=0.5, iterations=2):
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.order = order
        self.delay = delay
        self.frame_size = frame_size
        self.overlap = overlap
        self.iterations = iterations
        self.frame_shift = int(self.frame_size - np.floor(self.overlap * self.frame_size))
        self.time_data = None
        self.frequency_data = None
        self.fs = None

    def run(self):
        '''Run echo removal'''
        self.__frequency_domain_variance_normalized_delayed_liner_prediction()

    def __frequency_domain_variance_normalized_delayed_liner_prediction(self):
        # Run Short Time Fourier Transform on input time domain data
        self.frequency_data = self.__normalized_stft()

        # Get number of frequency band
        self.freq_num = self.frequency_data.shape[-1]

        # Select only the required amount of output channels for farther processing
        drv_freq_data = self.frequency_data[0:self.out_channels].copy()

        # Run Variance Normalized Delayed Linear Prediction for each frequency band
        for i in range(self.freq_num):
            # Select one frequency band and transpose the matrix
            freq_bin = self.frequency_data[:, :, i].T

            # Run varience normalized delayed linear prediction on the selected band
            dk = self.__variance_normalized_delayed_liner_prediction(freq_bin)

            # Transpose the dereverberated frequency band and store it
            drv_freq_data[:, :, i] = dk.T
        self.frequency_data = drv_freq_data

        # Run Inverse Short Time Fourier Transform on output frequency domain data
        self.time_data = self.__normalized_istft()
        return self.time_data

    def __variance_normalized_delayed_liner_prediction(self, xk):
        columns = xk.shape[0] - self.delay

        # Select only output number of channels
        xk_buf = xk[:, 0:self.out_channels]

        # Add zero padding and flip
        xk = np.flip(np.concatenate((np.zeros((self.order - 1, self.in_channels)), xk), axis=0),
                     axis=1).copy()

        # Reshape data with moving window (only a view to data for performance benefit)
        frames = stride_tricks.as_strided(xk, shape=(self.in_channels * self.order, columns),
                                          strides=(xk.strides[-1],
                                                   xk.strides[-1] * self.in_channels))

        frames = np.flip(frames, axis=0)
        mean_inv_buff = np.mean(1 / np.power(np.abs(xk_buf[self.delay:]), 2), axis=1)
        for _ in range(self.iterations):
            # Calculate covarient matrices
            mat_m = np.dot(np.dot(frames, np.diag(mean_inv_buff)), np.conj(frames.T))
            mat_n = np.dot(frames, np.conj(xk_buf[self.delay:] * mean_inv_buff.reshape(-1, 1)))
            # Calculate coefficients
            coeffs = np.dot(np.linalg.inv(mat_m), mat_n)
            # Subtraction of calculated values from frequency band
            dk = xk_buf[self.delay:] - np.dot(frames.T, np.conj(coeffs))
            mean_inv_buff = np.mean(1 / np.power(np.abs(dk), 2), axis=1)
        return np.concatenate((xk_buf[0:self.delay], dk))

    def __normalized_stft(self):
        '''Multi channel normalized Short Time Fourrier Transform'''
        columns = int(np.ceil((self.time_data.shape[1] - self.frame_size) / self.frame_shift)) + 1

        # Add zero padding to the time data
        data = np.concatenate((self.time_data, np.zeros((self.time_data.shape[0], self.frame_shift),
                               dtype=np.float32)), axis=1).copy()

        # Normalize data
        data = data / np.max(np.abs(data))

        # Reshape data with moving window (only a view to data for performance benefit)
        frames = stride_tricks.as_strided(data, shape=(data.shape[0], columns, self.frame_size),
                                          strides=(data.strides[-2],
                                                   data.strides[-1] * self.frame_shift,
                                                   data.strides[-1])).copy()

        # Multiple all the frames  with hanning window
        frames *= np.hanning(self.frame_size)

        # Run RFFT and return data in frequency domain
        return np.fft.rfft(frames)

    def __normalized_istft(self):
        '''Multi channel Inverse Short Time Fourrier Transform'''
        # Run Inverse RFFT
        time_data = np.fft.irfft(self.frequency_data)

        # Calculate frame count
        frame_num = self.frequency_data.shape[-2]

        # Calculate length of all data
        length = (frame_num - 1) * self.frame_shift + self.frame_size

        # Create placeholder for output data
        output = np.zeros((self.frequency_data.shape[0], length))

        # Place output data in correct format into output array
        for output_idx, time_idx in zip(range(0, frame_num * self.frame_shift, self.frame_shift),
                                        range(frame_num)):
            output[:, output_idx:output_idx + self.frame_size] += time_data[:, time_idx]

        # Return normalized data
        return output / np.max(np.abs(output))

    def load(self, file):
        '''Loads audio file'''
        if not os.path.isfile(file):
            print('The file does not exist')
            return

        data, self.fs = sf.read(file, always_2d=True)
        self.time_data = data.T[0:self.in_channels].copy()
        return self.time_data, self.fs

    def write(self, dir, file, data=None, fs=None):
        '''Save output data with sampling frequency into folder'''
        if not os.path.exists(dir):
            os.makedirs(dir)

        path = os.path.join(dir, file)

        if data is None:
            data = self.time_data

        if fs is None:
            fs = self.fs

        sf.write(path, data.T, fs, subtype='PCM_16')
        return path


def restricted_float(x):
    try:
        x = float(x)
    except ValueError:
        raise argparse.ArgumentTypeError("%r not a floating-point literal" % (x,))

    if x < 0.01 or x > 0.99:
        raise argparse.ArgumentTypeError("%r not in range [0.01, 0.99]" % (x,))
    return x
