import argparse
import os

from utils.utils import DeReverb, restricted_float


def main(args):
    dereverb = DeReverb()
    dereverb.config(args.in_ch, args.out_ch, args. order, args.delay,
                    args.frame, args.overlap, args.iterations)
    dereverb.load(args.filename)
    dereverb.run()
    dereverb.write(os.path.dirname(args.filename), 'out_{}'.format(os.path.basename(args.filename)))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Echo removal from audio files.')

    parser.add_argument('filename', help='input audio file with echo')
    parser.add_argument('-in_ch', default=2, help='number of input channels',
                        type=int, choices=range(2, 9))
    parser.add_argument('-out_ch', default=2, help='number of output channels',
                        type=int, choices=range(2, 9))
    parser.add_argument('-delay', default=2, help='prediction delay', type=int)
    parser.add_argument('-order', default=30, help='prediction order', type=int)
    parser.add_argument('-frame', default=512, help='frame size', type=int)
    parser.add_argument('-overlap', default=0.5, help='overlap between frames',
                        type=restricted_float)
    parser.add_argument('-iterations', default=2, help='iterations', type=int)
    args = parser.parse_args()

    main(args)
